require_relative 'branch_mapper.rb'
require_relative 'truthtable.rb'

class Minimizator
  def initialize(shortest_paths)
    @shortest_paths = shortest_paths
    @minimization_function = nil
  end

  def call
    map_branch_values_to_truth_table
    minimize
    prepare_output_expression
  end

  private

  attr_reader :shortest_paths

  def map_branch_values_to_truth_table
    @branch_mapper = BranchMapper.new(shortest_paths.flatten.uniq).call
  end

  def prepare_input_function
    shortest_paths.map do |shortest_path|
      shortest_path.map do |branch|
        @branch_mapper[branch]
      end.join('&')
    end.join('|')
  end

  def minimize
    @minimized_function = TruthTable.new { |v| eval(prepare_input_function) }.dnf
  end

  def prepare_output_expression
    @minimized_function.gsub!(/\d+/) { |num| num.to_i + 1 }.gsub!(/v\[(\d)\]/, 'e\1')
  end
end
