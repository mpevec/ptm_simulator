class BranchMapper
  def initialize(branches)
    @branches = branches
    @mapped_branches = {}
  end

  def call
    branches.map do |branch|
      mapped_branches[branch] = map_branch(branch)
    end

    mapped_branches
  end

  private

  attr_reader :branches, :mapped_branches

  def map_branch(branch)
    "v[#{branch[1..-1].to_i - 1}]"
  end
end
