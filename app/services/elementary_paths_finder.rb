class ElementaryPathsFinder
  def initialize(graph, start_node, end_node)
    @graph = graph
    @start_node = start_node
    @end_node = end_node
  end

  def call
    @elementary_paths = find_all_paths(start_node, end_node)

    map_paths_to_links
  end

  private

  attr_reader :graph, :start_node, :end_node, :elementary_paths, :links

  def find_all_paths(a, b, result = [], end_result = [])
    result = result+[a]
    end_result << result if a == b
    graph.neighbours[a].each do |v|
      find_all_paths(v, b, result, end_result) if ! result.include?(v)
    end

    end_result
  end

  def map_paths_to_links
    @elementary_paths.map do |path|
      path.each_cons(2).to_a.map do |nodes|
        graph.links[nodes]
      end
    end
  end
end
