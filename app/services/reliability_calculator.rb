class ReliabilityCalculator
  def initialize(graph, start_node, end_node)
    @graph = graph
    @start_node = start_node
    @end_node = end_node
  end

  def call
    elementary_paths = find_elementary_paths
    calculation_expression = Minimizator.new(elementary_paths).call

    puts calculate(calculation_expression)
  end

  private

  attr_reader :start_node, :end_node, :graph

  def find_elementary_paths
    ElementaryPathsFinder.new(graph, start_node, end_node).call
  end

  def calculate(expression)
    expression.split(' | ').inject(0){|sum, element| sum + reliability_total(element) }
  end

  def reliability_total(element)
    total = 1
    element.split('&').each do |branch|
      if branch[0] == '!'
        total *= (1 - reliability(branch[1..-1]))
      else
        total *= reliability(branch)
      end
    end

    total
  end

  def reliability(branch)
    @graph.links_reliabilities[branch.to_sym]
  end
end
