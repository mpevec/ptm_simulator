class Schema < ApplicationRecord
  belongs_to :user
  serialize :elements

  # Converting to JSON to Hash
  before_save { |schema|

    schema.elements =
        JSON.parse(schema.elements.to_s) unless schema.elements.is_a?(Hash)
  }
end
