class Graph
  def initialize

  end

  def neighbours
    { "v1"=>["v2", "v3"],
      "v2"=>["v1", "v3", "v4"],
      "v3"=>["v1", "v2", "v4"],
      "v4"=>["v2", "v3"] }
  end

  def links
    { ['v1', 'v2'] => 'e1',
      ['v2', 'v1'] => 'e1',
      ['v2', 'v4'] => 'e2',
      ['v4', 'v2'] => 'e2',
      ['v1', 'v3'] => 'e3',
      ['v3', 'v1'] => 'e3',
      ['v3', 'v4'] => 'e4',
      ['v4', 'v3'] => 'e4',
      ['v2', 'v3'] => 'e5',
      ['v3', 'v2'] => 'e5' }
  end

  def links_reliabilities
    { 'e1': 0.85,
      'e2': 0.85,
      'e3': 0.85,
      'e4': 0.85,
      'e5': 0.85 }
  end
end
