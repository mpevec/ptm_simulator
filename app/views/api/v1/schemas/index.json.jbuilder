json.schemas @schemas do |schema|
  json.partial! 'schema', schema: schema
end