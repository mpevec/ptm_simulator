# PTM Simulator Backend #

U ovom repozitoriju nalazi se stražnji dio sustava simulatora.

### Postavljanje ###

* Instalirati [Ruby on Rails](https://www.tutorialspoint.com/ruby-on-rails/rails-installation.htm) 
* Klonirati repozitorij i pozicionirati se u njega
* `gem install bundle` Instaliranje bundlera ako već nije instaliran
*  `bundle install` Instaliranje potrebnih alata o kojima projekt ovisi
* `rails s` Pokretanje servera na defaultnom portu 3000 
* `rails s -p 3636` Pokretanje servera na portu 3636 

### Who do I talk to? ###

Maintainers:

* Nika Jukić (nika.jukic@fer.hr)
* Matija Pevec (matija.pevec@fer.hr)